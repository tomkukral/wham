package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"k8s.io/api/admission/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func init() {
	readConfig()
}

func serveCheckLive(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func validate(req v1beta1.AdmissionReview) *v1beta1.AdmissionResponse {
	r := &v1beta1.AdmissionResponse{
		Result: &metav1.Status{},
	}

	if currentConfig.RefuseAll {
		r.Allowed = false
		r.Result.Message = "RefuseAll is enabled, everything is refused."
	}

	return r
}

func serveValidate(w http.ResponseWriter, r *http.Request) {
	var body []byte
	var ctr = "application/json"

	// read body
	if r.Body != nil {
		if data, err := ioutil.ReadAll(r.Body); err == nil {
			body = data
		}
	}

	// verify the content type is accurate
	contentType := r.Header.Get("Content-Type")
	if contentType != ctr {
		log.Printf("Expectected %s, got %s", ctr, contentType)
		return
	}

	fmt.Printf("Handling request %s", body)

	// prepare objects
	req := v1beta1.AdmissionReview{}

	err := json.Unmarshal(body, &req)
	if err != nil {
		log.Printf("Failing unmarshal to AdmissionRever: %s", err)
	}

	response := validate(req)

	// marshal response
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println(err)
	}

	if _, err := w.Write(resp); err != nil {
		log.Println(err)
	}
}

func main() {
	r := mux.NewRouter()

	// register handlers
	r.HandleFunc("/check/live", serveCheckLive)
	r.HandleFunc("/validate", serveValidate)

	// log requests
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)

	server := &http.Server{
		Addr:    currentConfig.Listen,
		Handler: loggedRouter,
	}
	log.Printf("starting wham on %s", currentConfig.Listen)

	log.Fatal(server.ListenAndServe())
}
