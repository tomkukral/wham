package main

import (
	"log"

	"github.com/spf13/viper"
)

var currentConfig config

type config struct {
	// listen on this socket
	Listen string

	// RefuseAll will report everything as invalid
	RefuseAll bool
}

func readConfig() {
	viper.SetConfigName("wham")
	viper.AddConfigPath("/etc/wham")

	viper.SetDefault("Listen", ":4430")
	viper.SetDefault("RefuseAll", false)
	err := viper.ReadInConfig()
	if err != nil {
		log.Printf("Unable to read config file, using defaults: %s", err)
	}

	viper.Unmarshal(&currentConfig)
}
